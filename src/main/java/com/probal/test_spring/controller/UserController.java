package com.probal.test_spring.controller;


import com.probal.test_spring.model.User;
import com.probal.test_spring.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;

@RestController
public class UserController {

    @Autowired
    private UserService userService;


    @GetMapping("/save_users")
    public List<User> addUsers() {
        List<User> userList = Arrays.asList(
                new User("Mahabub"),
                new User("Rashid"),
                new User("Rihan")
        );

        return userService.addUsers(userList);
    }

    @GetMapping("/users")
    public List<User> getUsers() {
        return userService.getUsers();
    }
}
