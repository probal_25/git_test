package com.probal.test_spring.service;


import com.probal.test_spring.model.User;
import com.probal.test_spring.repository.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {

    @Autowired
    private UserRepo userRepo;

    public List<User> getUsers() {
        return userRepo.findAll();
    }

    public List<User> addUsers(List<User> userList) {
        return userRepo.saveAll(userList);
    }
}
